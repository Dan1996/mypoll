from .models import Poll, Choice
from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import authenticate, login
from django.views.generic import View
from .forms import UserForm, LoginForm

# def index(request):
#     all_polls = Poll.objects.all()
#     context = {
#         'all_polls': all_polls,
#     }
#     return render(request, 'pollsapp/index.html', context)
#
#
# def detail(request, poll_id):
#     poll = get_object_or_404(Poll, id=poll_id)
#     return render(request, 'pollsapp/detail.html', {'poll': poll})


def vote(request, poll_id):
    poll = get_object_or_404(Poll, id=poll_id)
    try:
        selected_choice = poll.choice_set.get(id=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'pollsapp/detail.html', {'poll': poll, 'error_message': 'Please select an answear'})
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return render(request, 'pollsapp/detail.html', {'poll': poll})


class IndexView(generic.ListView):
    template_name = 'pollsapp/index.html'
    context_object_name = 'all_polls'

    def get_queryset(self):
        return Poll.objects.all()


class DetailView(generic.DetailView):
    model = Poll
    template_name = 'pollsapp/detail.html'


class PollCreate(CreateView):
    model = Poll
    fields = ['poll_title', 'pub_date']


class PollUpdate(UpdateView):
    model = Poll
    fields = ['poll_title', 'pub_date']


class AlbumDelete(DeleteView):
    model = Poll
    success_url = reverse_lazy('pollsapp:index')


class UserRegistrationView(View):
    form_class = UserForm
    template_name = 'pollsapp/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            user = form.save(commit=False)
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()
            return redirect('pollsapp:index')

        return render(request, self.template_name, {'form': form})


class UserLoginView(View):
    form_class = LoginForm
    template_name = 'pollsapp/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request=request, username=username, password=password)

            if user is None:
                form.add_error(None, 'user/pass combination does not add upp.')
            elif user.is_active:
                login(request, user)
                return redirect('pollsapp:index')

        return render(request, self.template_name, {'form': form})










