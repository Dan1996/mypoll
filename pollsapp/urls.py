from django.conf.urls import url
from . import views

app_name = 'pollsapp'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^register/$', views.UserRegistrationView.as_view(), name='register'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'add/$', views.PollCreate.as_view(), name='poll-add'),
    url(r'^(?P<poll_id>[0-9]+)/vote', views.vote, name='vote'),
    url(r'^login/$', views.UserLoginView.as_view(), name='login')
]
