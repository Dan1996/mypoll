from django.db import models
from django.core.urlresolvers import reverse


class Poll(models.Model):
    def __str__(self):
        return self.poll_title

    def get_absolute_url(self):
        return reverse('pollsapp:detail', kwargs={'pk': self.pk})

    poll_title = models.CharField(max_length=250)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    def __str__(self):
        return self.choice_text

    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
